//
//  ViewController.swift
//  VATableViewWithDiffiableDataSource
//
//  Created by Vikash Anand on 26/11/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let cellId = "sampleCell"
    enum Section {
        case main
    }
    
    let tableView = UITableView()
    var dataSource: UITableViewDiffableDataSource<Section, Movies1994>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.tableView)
        
        NSLayoutConstraint.activate([
            self.tableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.view.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: self.tableView.trailingAnchor),
            self.view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: self.tableView.bottomAnchor)
        ])
        
        self.configureDatasource()
        self.populateDataSource(animated: true)
    }
    
    private func configureDatasource() {
        self.dataSource = UITableViewDiffableDataSource<Section, Movies1994>(tableView: self.tableView, cellProvider: { (tableView, indexPath, movie1994) -> UITableViewCell? in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath)
            cell.textLabel?.text = movie1994.name
            return cell
        })
    }
    
    private func populateDataSource(animated: Bool) {
        
        var snapshot = NSDiffableDataSourceSnapshot<Section, Movies1994>()
        snapshot.appendSections([.main])
        snapshot.appendItems([Movies1994(name: "Forest Gump", category: "Drama")], toSection: .main)
        snapshot.appendItems([Movies1994(name: "Swashank Redumption", category: "Drama")], toSection: .main)
        snapshot.appendItems([Movies1994(name: "Dumb and dumber", category: "Comedy")], toSection: .main)
        snapshot.appendItems([Movies1994(name: "Speed", category: "Action")], toSection: .main)
        
        self.dataSource.apply(snapshot, animatingDifferences: animated)
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if let selectedMovie = self.dataSource.itemIdentifier(for: indexPath) {
            var currentSnapShot = self.dataSource.snapshot()
            currentSnapShot.deleteItems([selectedMovie])
            self.dataSource.apply(currentSnapShot, animatingDifferences: true)
        }
    }
}
