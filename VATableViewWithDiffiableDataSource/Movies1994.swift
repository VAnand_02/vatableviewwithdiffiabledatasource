//
//  Movies1994.swift
//  VATableViewWithDiffiableDataSource
//
//  Created by Vikash Anand on 26/11/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import Foundation

struct Movies1994: Hashable {
    let name: String
    let category: String
    let identifier: UUID = UUID()
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    
    static func == (left: Movies1994, right: Movies1994) -> Bool {
        left.identifier == right.identifier
    }
}
